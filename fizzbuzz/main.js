const FIZZ_DIVISOR = 3;
const BUZZ_DIVISOR = 5;

const OutputType = {
  FIZZ: "Fizz",
  BUZZ: "Buzz",
  FIZZ_BUZZ: "FizzBuzz",
};

function fizzBuzz(n) {
  for (let i = 1; i <= n; i++) {
    const isFizz = i % FIZZ_DIVISOR === 0;
    const isBuzz = i % BUZZ_DIVISOR === 0;

    let output = "";

    if (isFizz) output += OutputType.FIZZ;
    if (isBuzz) output += OutputType.BUZZ;

    console.log(output || i);
  }
}
