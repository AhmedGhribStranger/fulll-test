class ParkVehicleUseCase {
  constructor(fleetRepository) {
    this.fleetRepository = fleetRepository;
  }

  execute(vehicle, location, user) {
    const fleet = this.fleetRepository.getFleetByUser(user);

    if (!fleet.hasVehicle(vehicle)) {
      throw new Error("Vehicle not registered");
    }

    if (
      vehicle.lastParkedLocation &&
      vehicle.lastParkedLocation.equals(location)
    ) {
      throw new Error("Vehicle already parked at this location");
    }

    vehicle.lastParkedLocation = location;
  }
}

module.exports = ParkVehicleUseCase;
