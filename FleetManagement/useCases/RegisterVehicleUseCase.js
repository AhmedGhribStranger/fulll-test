class RegisterVehicleUseCase {
  constructor(fleetRepository) {
    this.fleetRepository = fleetRepository;
  }

  execute(vehicle, user) {
    const fleet = this.fleetRepository.getFleetByUser(user);

    if (!fleet.hasVehicle(vehicle)) {
      fleet.addVehicle(vehicle);
    } else {
      throw new Error("Vehicle already registered");
    }
  }
}

module.exports = RegisterVehicleUseCase;
