const User = require("./entities/User");
const Vehicle = require("./entities/Vehicle");
const Location = require("./entities/Location");
const FleetRepository = require("./repositories/FleetRepository");
const RegisterVehicleUseCase = require("./useCases/RegisterVehicleUseCase");
const ParkVehicleUseCase = require("./useCases/ParkVehicleUseCase");

describe("Vehicle Fleet Management", () => {
  let fleetRepository;
  let registerVehicleUseCase;
  let parkVehicleUseCase;
  let user1;
  let user2;
  let vehicle1;
  let location1;

  beforeEach(() => {
    fleetRepository = new FleetRepository();
    registerVehicleUseCase = new RegisterVehicleUseCase(fleetRepository);
    parkVehicleUseCase = new ParkVehicleUseCase(fleetRepository);
    user1 = new User("1", "John Doe");
    user2 = new User("2", "Sam Altman");
    vehicle1 = new Vehicle("1", "Car");
    location1 = new Location({ lat: 40.7128, lon: -74.006 });
  });

  // Feature 1. Scenario 1: I can register a vehicle
  test("Registering a vehicle adds it to the fleet", () => {
    registerVehicleUseCase.execute(vehicle1, user1);
    const fleet = fleetRepository.getFleetByUser(user1);
    expect(fleet.hasVehicle(vehicle1)).toBe(true);
  });

  // Feature 1. Scenario 2: I can't register the same vehicle twice
  test("Attempting to register the same vehicle twice throws an error", () => {
    registerVehicleUseCase.execute(vehicle1, user1);
    expect(() => registerVehicleUseCase.execute(vehicle1, user1)).toThrow(
      "Vehicle already registered"
    );
  });

  // Feature 1. Scenario 3: Same vehicle can belong to more than one fleet
  test("The same vehicle can belong to more than one fleet", () => {
    registerVehicleUseCase.execute(vehicle1, user2);
    const fleet1 = fleetRepository.getFleetByUser(user1);
    const fleet2 = fleetRepository.getFleetByUser(user2);
    expect(fleet1.hasVehicle(vehicle1)).toBe(false);
    expect(fleet2.hasVehicle(vehicle1)).toBe(true);
  });

  // Feature 2. Scenario 1: Successfully park a vehicle
  test("Successfully parking a vehicle updates its location", () => {
    registerVehicleUseCase.execute(vehicle1, user1);
    parkVehicleUseCase.execute(vehicle1, location1, user1);
    const fleet = fleetRepository.getFleetByUser(user1);
    const parkedVehicle = fleet.vehicles.find((v) => v.id === vehicle1.id);
    expect(parkedVehicle.lastParkedLocation).toEqual(location1);
  });

  // Feature 2. Scenario 2: Can't localize a vehicle to the same location two times in a row
  test("Attempting to park a vehicle at the same location twice throws an error", () => {
    registerVehicleUseCase.execute(vehicle1, user1);
    parkVehicleUseCase.execute(vehicle1, location1, user1);
    expect(() =>
      parkVehicleUseCase.execute(vehicle1, location1, user1)
    ).toThrow("Vehicle already parked at this location");
  });
});
