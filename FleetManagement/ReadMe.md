## Step 1: Implementing Step 1 and Step 2 following clean architecture principles

### Clean Architecture Principles

I've adhered to Clean Architecture principles by organizing the code into distinct layers: `Entities`, `Repository`, `Use Cases`, and `App`. The entities encapsulate the core domain logic, the repository handles data access, use cases encapsulate application logic, and the app orchestrates the use cases and dependencies.

## Testing

I've incorporated automated testing using [Jest](https://jestjs.io/) to ensure the correctness and reliability of the implemented functionality. The test suite covers various scenarios, validating the behavior of the vehicle fleet management system.

To run the tests, you can use the following command:

```bash
npm test
```

### Notes

- **No Frameworks Used**: Following Step 1 guidelines, I avoided using any frameworks.
- **In-Memory Storage**: The fleet repository is currently an in-memory storage solution, as specified in the guidelines.

## Step 2: Exposing the work and persisting the data in a database

## Command-Line Interface

I've created a simple command-line interface (CLI) to expose the functionality to the users. The `fleet_cli.js` script parses the command-line arguments and delegates the execution to corresponding functions in the `commands.js` module.

## Database Integration

To persist data, I've integrated MongoDB as the database. The `commands.js` module connects to the MongoDB database using Mongoose, defines schemas for `Location`, `Vehicle`, and `Fleet`, and implements functions to perform the specified actions.

## Making fleet_cli.js executable

```shell
chmod +x cli.js
```

# Test 1: Creating a Fleet

## Command:

```shell
./fleet_cli.js create user123
```

## Output:

Fleet created with ID: 657eda7abeffd99519d6ce3c

## Explanation

This test creates a new fleet for the user with ID "user123".
The CLI script outputs the confirmation message with the newly created fleet ID.

# Test 2: Registering a Vehicle

## Command:

```shell
./fleet_cli.js register-vehicle 657eda7abeffd99519d6ce3c ABC123
```

## Output:

Vehicle registered successfully

## Explanation

This test registers a vehicle with the plate number "ABC123" to the fleet with ID "657eda7abeffd99519d6ce3c".
The CLI script outputs a success message upon successful registration.

# Test 3: Localizing a Vehicle

## Command:

```shell
./fleet_cli.js localize-vehicle 657eda7abeffd99519d6ce3c ABC123 40.7128 -74.0060 0
```

## Output:

Vehicle localized successfully

## Explanation

This test updates the location of the vehicle with the plate number "ABC123" in the fleet with ID "657eda7abeffd99519d6ce3c".
The CLI script outputs a success message upon successful localization.
